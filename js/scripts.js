var Arreglo = [];

function crearArreglo(n) { //funcion para generar numeros enteros aleatorios
    for (i = 0; i < n; i++) {
        Arreglo[i] = Math.round(Math.random() * 100);
    }
    return Arreglo;
}

Arreglo = crearArreglo(20); //en los parentesis va la cantidad de numeros a generar
console.log("El arreglo es:", Arreglo.join()); 



function promedio(Arreglo) { //funcion para sacar el promedio
    var suma = 0;
    for (i = 0; i < Arreglo.length; i++) {
        suma += Arreglo[i];
    }

    console.log("La suma es:", suma);
    return suma / Arreglo.length ;
}

console.log("El promedio es:", promedio(Arreglo));

function comparar(a, b) { //funcion para comparar
    return b - a;
}

function ordenar(Arreglo) { //funcion para ordenar el arreglo con ayuda de sort
    Arreglo.sort(comparar);
}

ordenar(Arreglo);
console.log("El arreglo ordenado de mayor a menor es :", Arreglo.join());
var orden = Arreglo.join();

function pares(Arreglo) { //funcion para contar los numeros pares 
    let pares = 0;
    for (let i = 0; i < Arreglo.length; i++) {
        if (Arreglo[i] % 2 == 0) {
            pares++;
        }
    }
    return pares;
}

console.log("La cantidad de pares son:", pares(Arreglo));



function Ejecutar(){
    document.getElementById('marreglo').innerHTML = document.getElementById('marreglo').innerHTML + Arreglo;
    
    document.getElementById('promarreglo').innerHTML = document.getElementById('promarreglo').innerHTML + promedio(Arreglo);
    
    document.getElementById('oarreglo').innerHTML = document.getElementById('oarreglo').innerHTML + orden;
    
    document.getElementById('pararreglo').innerHTML = document.getElementById('pararreglo').innerHTML + pares(Arreglo);
    
}